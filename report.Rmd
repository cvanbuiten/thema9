---
title: "The effect of nutritional composition on Eosinophilic Esophagitis."
author: "Carlo van Buiten"
date: "11/16/2020"
output:
  pdf_document: default
  html_document:
    df_print: paged
linkcolor: blue
---

```{r, echo=FALSE, out.width='100%', fig.cap="Patient suffering from an inflamed esophagus. \\cite{EoE}", fig.align='center'}
knitr::include_graphics('esophagitis.png')
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
```

\newpage
\tableofcontents
\newpage

# Introduction
## Theory
A common cause for Eosinophilic Esophagitis, which is essentially the inflammation of the esophagus (otherwise known as EoE) is food allergy. While it is obvious that the elimination of the allergens from the diet of a patient can help avoid an allergic reaction, it was hypothesized that a neocate (amino acid based) diet might also be a factor in increasing or decreasing risk of EoE. This combined with the general effects of nutritional composition is an interesting subject for research.

## Objective
The data, which has been provided by the Academisch Medisch Centrum (AMC)\cite{AMC} from Amsterdam, contains the dietary data of 40 patients with values for both the start of the program and after a period of six weeks. It also includes the histological responses of the patients to their diets. This data is to be used to train a machine learning model with the ultimate goal of answering the question: What do the effects of nutritional composition on intestinal allergic reaction appear to be when examined through the use of machine learning?  
If such a model were to be able to accurately predict the histological responses of patients based on their diets, we could identify the key nutrients and foods that effect the progression of EoE.

\newpage

# Materials & Methods
The code and methodology described in this section can be found in the following two bitbucket repositories:  
[Research Repository](https://bitbucket.org/cvanbuiten/thema9/src/master/) (R Visualizations and logs).  
[Code Repository](https://bitbucket.org/cvanbuiten/javawrapper/src/master/) (Java wrapper for the (re-)application of the machine learning model).

## Materials
The data was proved by the AMC\cite{AMC}, it was part of a research they were conducting on EoE\cite{EoE}. The original dataset contained contained the data of 40 patients (it was actually 79 entries long, but 39 were completely empty), each of these patients had over 800 attributes which was too much to effectively use machine learning so only Neocate and the features that correlated highly (and were not starting values) with histological response were selected. These features were; Vitamin C intake after six weeks, folate intake after six weeks, nicotinacid intake after six weeks and folateequivalent after six weeks.  
This feature selecting was accomplished through the use of R and these features were then to be used by machine learning algorithms in Weka. The final model was then wrapped inside an application through the use of Java.

## Methods
In an attempt to find a model that could accurately predict histological response based on the data many algorithms were used. Initially, not a single algorithm could get an accuracy that was any higher than 50%, but after trimming more features (Potassium intake after six weeks and two features whose meaning was unclear and undocumented) slight improvements could be seen in both the Naive Bayes algorithm and OneR, with OneR beating Naive Bayes at 60% accuracy. Further experimenting did not lead to any more improvements and as such OneR (with 10-fold cross-validation) ended up being used. This model was then wrapped inside a Java application which can reapply the model to new data in order to classify it.

\newpage

# Results
One way to describe the effectiveness of a certain treatment is by looking at the histological response (simply put: improved appearance of the affected tissue) of patients because a strong histological response often indicates the prognosis will imrove as well. So let us have a look at what the spread of histological response looks like among our patients.

```{r, out.width='60%', fig.cap="Histological response compared between regular FFED and FFED + Neocate.", fig.align='center'}
library(haven)
library(ggplot2)
library(knitr)
library(dplyr)

#Loading in data
data <- read_sav("projectdata.sav")

#Filter out NA columns
data <- data[, colSums(is.na(data)) != nrow(data)]
#Filter out NA rows
data <- data[rowSums(is.na(data)) != ncol(data),]
#Alter data types to make them easier to work with
data$Neocate <- as.factor(data$Neocate)
data$Histological_Response <- as.factor(data$Histological_Response)

#Compare levels of histological response between standard and Neocate diets
response <- select(data, 1, 2, Histological_Response, Gender)
#Filter out NA rows
response <- response[rowSums(is.na(response)) != ncol(response),]
#Prepare data for plotting
response_count <- count(response, Neocate, Histological_Response)

#Plot the histological response against the diet type
ggplot(response_count, aes(x=Neocate, y=n,fill=Histological_Response)) +
  geom_bar(stat="identity", position = position_dodge())+
  geom_text(aes(label=n), vjust=1.6, color="white",
            position = position_dodge(0.9), size=4)+
  ylab("Occurences")+
  xlab("Diet")+
  scale_fill_discrete(name="Histological Response", 
                      labels =c("No Response", "Partial Response", "Full Response"))+
  scale_x_discrete(labels=c("FFED", "FFED + Neocate"))
```

As can be seen in figure 1, while both diets show a strong response in a number of patients, the neocate diet shows considerably better results than the default food elimination diet with double the amount of patients showing a full histological response and 5 less patients showing only a partial response or no response at all. This does seem to indicate that a neocate diet could be instrumental in the protection of patients against EoE. However, it is worth mentioning that men and women can differ vastly in biological terms. So what would the graph look like if we were to separate on gender?

```{r, out.width='60%', fig.cap="Histological response compared between the 2 diet types, separated by gender.", fig.align='center'}
#Prepare gender data
response_gender <- count(response, Neocate, Histological_Response, Gender)
response_gender$Gender <- as.factor(response_gender$Gender)

#Create wrapper labels
Neocate.labs <- c("FFED", "FFED + Neocate")
names(Neocate.labs) <- c(0, 1)

#Plot the histological response against the diet type, separating by gender
ggplot(response_gender, aes(x=Histological_Response, y=n, fill=Gender)) +
  geom_bar(stat="identity", position = "stack")+
  ylab("Occurences")+
  xlab("Response Severity")+
  facet_wrap(~ Neocate, labeller = labeller(Neocate = Neocate.labs))+
  scale_fill_discrete(name="Gender",
                      labels =c("Female", "Male"))
```

When comparing the histological response data between the genders, it seems that men have a better chance of displaying a full response with a regular elimination diet, and a similar chance of showing full response with a neocate diet. This is especially interesting considering the fact that these are raw counts and that the majority of the patients whose data was collected were female (24 out of 40). What is also interesting about this is that researchers have shown women to have a stronger immune system than men which means that the men that showed a full histological response with the regular FFED diet likely did not do so by virtue of their strong immune systems.

While histological response is a nice and easy way of quickly assessing overal effectiveness, the PEC value (a measure of how many white blood cells have accumulated in the tissue) is a more concrete way of describing severity of inflammation. And while both diets see a drop in PEC value after six weeks as can be seen in figure 3 and 4, the neocate variety sees a considerably larger drop than its standard FFED counterpart, almost halving in size. This is in line with the fact that the neocate diet appeared to cause a better histological response as well.

```{r, out.width='60%', fig.cap="Normalized white blood cell counts.", fig.align='center'}
#Prepare normalized PEC levels for plotting
peakeos <- aggregate(list(data$LN_PeakEosBaseline_Max, data$LN_PeakEosSixwk_Max), by = list(data$Neocate), mean)
colnames(peakeos) <- c("Neocate", "LN_Baseline", "LN_Sixwk")

#Plot the average normalized base PEC levels per diet
ggplot(peakeos, aes(x=Neocate, y=LN_Baseline, fill=Neocate))+
  geom_bar(stat="identity")+
  geom_text(aes(label=LN_Baseline), vjust=1.6, color="white",
            position = position_dodge(0.9), size=4)+
  ggtitle("Baseline PEC values")+
  ylab("LN PEC")+
  xlab("Diet")+
  scale_fill_discrete(labels=c("FFED", "FFED + Neocate"))

#Plot the avergage normalized PEC levels after six weeks per diet
ggplot(peakeos, aes(x=Neocate, y=LN_Sixwk, fill=Neocate))+
  geom_bar(stat="identity")+
  geom_text(aes(label=LN_Sixwk), vjust=1.6, color="white",
            position = position_dodge(0.9), size=4)+
  ggtitle("PEC values after six weeks")+
  ylab("LN PEC")+
  xlab("Diet")+
  scale_fill_discrete(labels=c("FFED", "FFED + Neocate"))
```

\newpage

The algorithm that was able to predict histological response was 10-fold cross-validation OneR with an accuracy of 60%.
```{r, echo=FALSE, out.width='100%', fig.cap="OneR model results.", fig.align='center'}
knitr::include_graphics('modelresults.png')
```
This means that out of 40 patients, 16 would not be accurately diagnosed based on their dietary data.  

When it comes to the diagnosis of diseases, however, false positives are less harmful than false negatives because these mean a patient does not get the medical attention they require. So let us look at the per class data.

```{r, echo=FALSE, out.width='100%', fig.cap="OneR model results per class.", fig.align='center'}
knitr::include_graphics('accuracyperclass.png')
```
As can be seen above, there is no redeeming factor in the per class cata, as the accuracy of no-response is the same and the partial-response accuracy is actually lower than the baseline response of 60%. This is problematic because patients that show no histological improvement are actually the ones in need of better treatment.  

Worth mentioning however, is that folate equivalent on its own accurately predicts 28 out of 40 cases, which is an accuracy of 70%. This indicates that monitoring folate equivalent intake can be a key factor in treating patients with EoE\cite{EoE}. The classifying results through the use of solely the folate equivalent attribute can be seen below.

```{r, echo=FALSE, out.width='100%', fig.cap="Folate equivalent as a predicting feature.", fig.align='center'}
knitr::include_graphics('folateequiv.png')
```

The generated model has been succesfully wrapped in a Java application that, when provided with new data, outputs that data with a newly classified histological response label added to each instance. Part of the output generated by the application using the test data can be seen below.

\newpage

```{r, echo=FALSE, out.width='70%', fig.cap="Application output for the test data, the rightmost column shows the predicted histological response. 0 = No, 1 = Partial, 2 = Full", fig.align='center'}
knitr::include_graphics('wrapperoutput.png')
```


\newpage

# Discussion & Conclusion
When looking at the histological response data, at first glance it seems very clear that FFED + Neocate outperforms FFED, with less of both partial and no responses and double the amount of full responses. Where it becomes interesting however, is when you look at the gender divide. Keeping in mind that there were fewer female participants than male, it stands to reason that when there are more men in a certain category than women, the respective difference between men and women would be even greater if there were equal the amount of men and women. The opposite is also true, if women trump men in a certain category this difference would be somewhat mitigated if there was an equal spread of men and women.  

It is curious how for regular FFED, there are more men than women with no response and there are also more men than women with full response. Whereas in the partial response category there are no men at all. Some men respond very well to an FFED diet, some men do not respond to it whatsoever, but not a single man shows a partial response to FFED.  

Moving over to the FFED + Neocate results where men seem to very clearly respond better than women. One can only speculate as to why this happens, since women have been shown to have stronger immune systems than men on average. One possible explanation is that this diet is actually detrimental to the immune response (in women) and that without it women would have actually outperformed men.  

The PEC values seem to very much point in the same direction as the histological responses, with FFED + Neocate clearly being the better performer. Note though, that the PEC values for neocate actually start a little lower than those of its counterpart. Considering the possibility that the reduction of the amount of white blood cells follows an exponential curve, it is possible that the patients on the neocate diet were actually, on average, a little further along in their recovery than those with the default FFED diet. 
Keep in mind that this exponentiality is a requirement for this to be possible, because if the dimishing of the cell count was linear, the differences before and after six weeks would be in the same order of magnitude, which is most definitely not the case here.  

The issue expressed above could be remedied by letting the experiment run for longer and gathering data at more regular intervals. This would also be helpful in general as you could gather data on complete recovery as well and potentially even on relapses.  
It would also be beneficial have a greater amount of test subjects as 40 is not too many on its own, but it also extremely limits the possibilities of trimming the dataset, for if outliers are cut the data set will shrink even more.  
It is also a little unclear as to how the experiment was conducted, if the patients were mainly left to their own devices with the diets being mere guidelines this could greatly impact the results due to a myriad of extra external factors potentially influencing the patient, including them making occassional exceptions to their diet. If this was the case it would be a worthwhile venture to conduct such an experiment in a more controlled fashion.  

It also seems like perhaps the data that was gathered by the AMC\cite{AMC} was not very well suited for predicting histological response at all, not just because there was a large amount of missing data, but even the highest correlating features look like the following graph when plotted against histological response.

```{r, echo=FALSE, out.width='100%', fig.cap="Nicotin acid plotted against histological response.", fig.align='center'}
knitr::include_graphics('nicotinacid.png')
```

\newpage

## Conclusion
While the 60% accuracy of the OneR model is not a very good accuracy for the diagnosis of a disease, the original goal of the AMC\cite{AMC} was not to predict disease, but to find the key nutrients to either take in or cut out of a diet to treat EoE\cite{EoE}. For this the 70% accuracy of folate equivalent is a good indication of importance. Looking at figure 8, there definitely seems to be an optimum intake for folate in the diets for the patients.  
These results, while not perfect, are certainly promising and give reason to believe that further research into the effect of folate intake could reveal more about how to treat patients with Eosinophilic Esophagitis.

\newpage

# Project Proposal
Application Design Minor  
Create a user-friendly application (preferably for handheld devices, Android, iOS etc.) for people with senstive esophagi that allows the user to input their intake of certain nutrients and foods through a streamlined and easy to use user-interface. The application should give the user feedback on what they should eat more of or what they have already eaten too much of (and should therefore avoid consuming for a while) in order to avoid (or be cured of) eosinophilic esophagitis. This user feedback should be easy to read and interpret for the user so that they can adjust their eating pattern without too much trouble. Think of graphs or other ways to clearly display these values for the user. Do not drown the user in complex numbers and values. Also think about how to make it easy for the user to input their intake data without too much trouble. The whole process should be as simple as possible.

\newpage


\begin{thebibliography}{9}

\bibitem{EoE}
\textit{EoE}. (2020). \textit{Kindergeneeskunde: EoE}. consulted on November 16th, 2020, from \\\texttt{https://www.vvkindergeneeskunde.be/}.

\bibitem{AMC}
\textit{AMC}. (2020). \textit{Academisch Medisch Centrum}.
\\\texttt{https://www.amc.nl/web/home.htm}.

\bibitem{Gastroenterology}
\textit{Department of Gastroenterology and Hepatology, Amsterdam University Medical Center, Amsterdam,  the Netherlands}

\end{thebibliography}