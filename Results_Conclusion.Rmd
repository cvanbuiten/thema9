---
title: "The effect of diet on eosinophilic esophagitis"
author: "Carlo van Buiten"
date: "9/27/2020"
output: pdf_document

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
```

\newpage
\tableofcontents
\newpage

## Results

One way to describe the effectiveness of a certain treatment is by looking at the histological response (simply put: improved appearance of the affected tissue) of patients because a strong histological response often indicates the prognosis will imrove as well. So let us have a look at what the spread of histological response looks like among our patients.

```{r, out.width='60%', fig.cap="Histological response compared between regular FFED and FFED + Neocate.", fig.align='center'}
library(haven)
library(ggplot2)
library(knitr)
library(dplyr)

#Loading in data
data <- read_sav("projectdata.sav")

#Filter out NA columns
data <- data[, colSums(is.na(data)) != nrow(data)]
#Filter out NA rows
data <- data[rowSums(is.na(data)) != ncol(data),]
#Alter data types to make them easier to work with
data$Neocate <- as.factor(data$Neocate)
data$Histological_Response <- as.factor(data$Histological_Response)

#Compare levels of histological response between standard and Neocate diets
response <- select(data, 1, 2, Histological_Response, Gender)
#Filter out NA rows
response <- response[rowSums(is.na(response)) != ncol(response),]
#Prepare data for plotting
response_count <- count(response, Neocate, Histological_Response)

#Plot the histological response against the diet type
ggplot(response_count, aes(x=Neocate, y=n,fill=Histological_Response)) +
  geom_bar(stat="identity", position = position_dodge())+
  geom_text(aes(label=n), vjust=1.6, color="white",
            position = position_dodge(0.9), size=4)+
  ggtitle("Response spread per diet")+
  ylab("Occurences")+
  xlab("Diet")+
  scale_fill_discrete(name="Histological Response", 
                      labels =c("No Response", "Partial Response", "Full Response"))+
  scale_x_discrete(labels=c("FFED", "FFED + Neocate"))
```

As can be seen in figure 1, while both diets show a strong response in a number of patients, the neocate diet shows considerably better results than the default food elimination diet with double the amount of patients showing a full histological response and 5 less patients showing only a partial response or no response at all. This does seem to indicate that a neocate diet could be instrumental in the protection of patients against EoE. However, it is worth mentioning that men and women can differ vastly in biological terms. So what would the graph look like if we were to separate on gender?

```{r, out.width='60%', fig.cap="Histological response compared between the 2 diet types, separated by gender.", fig.align='center'}
#Prepare gender data
response_gender <- count(response, Neocate, Histological_Response, Gender)
response_gender$Gender <- as.factor(response_gender$Gender)

#Create wrapper labels
Neocate.labs <- c("FFED", "FFED + Neocate")
names(Neocate.labs) <- c(0, 1)

#Plot the histological response against the diet type, separating by gender
ggplot(response_gender, aes(x=Histological_Response, y=n, fill=Gender)) +
  geom_bar(stat="identity", position = "stack")+
  ggtitle("Response spread per diet divided per gender")+
  ylab("Occurences")+
  xlab("Response Severity")+
  facet_wrap(~ Neocate, labeller = labeller(Neocate = Neocate.labs))+
  scale_fill_discrete(name="Gender",
                      labels =c("Female", "Male"))
```

When comparing the histological response data between the genders, it seems that men have a better chance of displaying a full response with a regular elimination diet, and a similar chance of showing full response with a neocate diet. This is especially interesting considering the fact that these are raw counts and that the majority of the patients whose data was collected were female (24 out of 40). What is also interesting about this is that researchers have shown women to have a stronger immune system than men which means that the men that showed a full histological response with the regular FFED diet likely did not do so by virtue of their strong immune systems.

While histological response is a nice and easy way of quickly assessing overal effectiveness, the PEC value (a measure of how many white blood cells have accumulated in the tissue) is a more concrete way of describing severity of inflammation. And while both diets see a drop in PEC value after six weeks as can be seen in figure 3 and 4, the neocate variety sees a considerably larger drop than its standard FFED counterpart, almost halving in size. This is in line with the fact that the neocate diet appeared to cause a better histological response as well.

```{r, out.width='60%', fig.cap="Normalized white blood cell counts.", fig.align='center'}
#Prepare normalized PEC levels for plotting
peakeos <- aggregate(list(data$LN_PeakEosBaseline_Max, data$LN_PeakEosSixwk_Max), by = list(data$Neocate), mean)
colnames(peakeos) <- c("Neocate", "LN_Baseline", "LN_Sixwk")

#Plot the average normalized base PEC levels per diet
ggplot(peakeos, aes(x=Neocate, y=LN_Baseline, fill=Neocate))+
  geom_bar(stat="identity")+
  geom_text(aes(label=LN_Baseline), vjust=1.6, color="white",
            position = position_dodge(0.9), size=4)+
  ggtitle("Baseline PEC values")+
  ylab("LN PEC")+
  xlab("Diet")+
  scale_fill_discrete(labels=c("FFED", "FFED + Neocate"))

#Plot the avergage normalized PEC levels after six weeks per diet
ggplot(peakeos, aes(x=Neocate, y=LN_Sixwk, fill=Neocate))+
  geom_bar(stat="identity")+
  geom_text(aes(label=LN_Sixwk), vjust=1.6, color="white",
            position = position_dodge(0.9), size=4)+
  ggtitle("PEC values after six weeks")+
  ylab("LN PEC")+
  xlab("Diet")+
  scale_fill_discrete(labels=c("FFED", "FFED + Neocate"))
```

\newpage

## Conclusion & Discussion

When looking at the histological response data, at first glance it seems very clear that FFED + Neocate outperforms FFED, with less of both partial and no responses and double the amount of full responses. Where it becomes interesting however, is when you look at the gender divide. Keeping in mind that there were fewer female participants than male, it stands to reason that when there are more men in a certain category than women, the respective difference between men and women would be even greater if there were equal the amount of men and women. The opposite is also true, if women trump men in a certain category this difference would be somewhat mitigated if there was an equal spread of men and women.  

It is curious how for regular FFED, there are more men than women with no response and there are also more men than women with full response. Whereas in the partial response category there are no men at all. Some men respond very well to an FFED diet, some men do not respond to it whatsoever, but not a single man shows a partial response to FFED.  

Moving over to the FFED + Neocate results where men seem to very clearly respond better than women. One can only speculate as to why this happens, since women have been shown to have stronger immune systems than men on average. One possible explanation is that this diet is actually detrimental to the immune response (in women) and that without it women would have actually outperformed men.  

The PEC values seem to very much point in the same direction as the histological responses, with FFED + Neocate clearly being the better performer. Note though, that the PEC values for neocate actually start a little lower than those of its counterpart. Considering the possibility that the reduction of the amount of white blood cells follows an exponential curve, it is possible that the patients on the neocate diet were actually, on average, a little further along in their recovery than those with the default FFED diet. 
Keep in mind that this exponentiality is a requirement for this to be possible, because if the dimishing of the cell count was linear, the differences before and after six weeks would be in the same order of magnitude, which is most definitely not the case here.  

The issue expressed above could be remedied by letting the experiment run for longer and gathering data at more regular intervals. This would also be helpful in general as you could gather data on complete recovery as well and potentially even on relapses.  
It would also be beneficial have a greater amount of test subjects as 40 is not too many on its own, but it also extremely limits the possibilities of trimming the dataset, for if outliers are cut the data set will shrink even more.  
It is also a little unclear as to how the experiment was conducted, if the patients were mainly left to their own devices with the diets being mere guidelines this could greatly impact the results due to a myriad of extra external factors potentially influencing the patient, including them making occassional exceptions to their diet. If this was the case it would be a worthwhile venture to conduct such an experiment in a more controlled fashion.